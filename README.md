# resource-agents-failover-script
Pacemaker resource agent that executes all scripts in the /usr/libexec/failoverscript/ directory

## Use it
* crm configure primitive script ocf:heartbeat:FailOverScript op monitor interval="30"
* pcs resource create FailOverScript ocf:heartbeat:FailOverScript  op monitor interval=10s
